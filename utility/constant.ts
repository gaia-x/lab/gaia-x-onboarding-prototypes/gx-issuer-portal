enum StatusType {
  FAIL = 'fail',
  LOADING = 'loading',
  SUCCESS = 'success',
  WAITING = 'waiting'
}

enum AlertType {
  DEFAULT = 'default',
  ERROR = 'error',
  INFO = 'info',
  SUCCESS = 'success',
}

export { StatusType, AlertType }
