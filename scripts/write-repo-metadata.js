#!/usr/bin/env node

const { execSync } = require('child_process')

process.stdout.write(
  JSON.stringify(
    {
      // eslint-disable-next-line global-require
      version: require('../package.json').version,
      commit:
        execSync('git rev-parse HEAD').toString().trim()
    }
  )
)
